package com.loanapp.Dao;

import java.util.List;
import java.util.Map;

import com.loanapp.model.OfferDetailsObject;

public interface CreateOfferDao {
	
	public OfferDetailsObject createOffer(OfferDetailsObject  offerDetailsObject);
	public List<Map<String, Object>> getOffer(String totalIncome);
}
