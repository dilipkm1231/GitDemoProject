package com.loanapp.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.loanapp.Dao.CreateOfferDao;
import com.loanapp.Service.CreateOfferService;
import com.loanapp.model.OfferDetailsObject;
import com.loanapp.model.UserDetailsObject;
@Service
public class CreateOfferServiceImpl implements CreateOfferService {

	@Autowired
	CreateOfferDao  createOfferDao;
	@Override
	public OfferDetailsObject createOffer(OfferDetailsObject offerDetailsObject) {
		createOfferDao.createOffer(offerDetailsObject);
		System.out.println("inside ServiceImpl");
		return null;
	}
	
	@Override
	public List<Map<String, Object>> getOffer(UserDetailsObject userdetails) {
		
			
		//Calcullate totel income 
		String totalIncome = userdetails.getFamilyIncome() + userdetails.getSalry() + userdetails.getOtherIncome();
		
		List<Map<String, Object>> offers=createOfferDao.getOffer(totalIncome);
		
		
		return offers;
	}
	

}
