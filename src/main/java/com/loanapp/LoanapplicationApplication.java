package com.loanapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.loanapp.*"})
public class LoanapplicationApplication {

	public static void main(String[] args) {
		System.out.println("Hello World");
		SpringApplication.run(LoanapplicationApplication.class, args);
		
	}
}
