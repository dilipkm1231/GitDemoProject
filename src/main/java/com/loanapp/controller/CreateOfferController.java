package com.loanapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loanapp.Service.CreateOfferService;
import com.loanapp.model.OfferDetailsObject;
import com.loanapp.model.UserDetailsObject;

@RestController
@RequestMapping("/loanApi")
@CrossOrigin("*")
public class CreateOfferController {
	
	@Autowired
	CreateOfferService createOfferService;
	
	@RequestMapping(value = "/createOffer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OfferDetailsObject> createOffer(@Valid @RequestBody OfferDetailsObject offer) {
		createOfferService.createOffer(offer);
		
		System.out.println("inside");
		return new ResponseEntity<>(offer, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/getOffer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDetailsObject> createOffer(@Valid @RequestBody UserDetailsObject userDetails) {
		
		System.out.println("inside getOffers");
		createOfferService.getOffer(userDetails);
		
		
		return new ResponseEntity<>(userDetails, HttpStatus.CREATED);
	}
	

}
