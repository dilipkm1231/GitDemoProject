package com.loanapp.model;

public class UserDetailsObject {
	String runningEmi;
	String fullName;
	String gender;
	String PAN;
	String address;
	String salry;
	String otherIncome;
	String familyIncome;
	public String getRunningEmi() {
		return runningEmi;
	}
	public void setRunningEmi(String runningEmi) {
		this.runningEmi = runningEmi;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPAN() {
		return PAN;
	}
	public void setPAN(String pAN) {
		PAN = pAN;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSalry() {
		return salry;
	}
	public void setSalry(String salry) {
		this.salry = salry;
	}
	public String getOtherIncome() {
		return otherIncome;
	}
	public void setOtherIncome(String otherIncome) {
		this.otherIncome = otherIncome;
	}
	public String getFamilyIncome() {
		return familyIncome;
	}
	public void setFamilyIncome(String familyIncome) {
		this.familyIncome = familyIncome;
	}
	
	
	
	
}
