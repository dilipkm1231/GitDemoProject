package com.loanapp.Service;

import java.util.List;
import java.util.Map;

import com.loanapp.model.OfferDetailsObject;
import com.loanapp.model.UserDetailsObject;

public interface CreateOfferService {

	public OfferDetailsObject createOffer(OfferDetailsObject  offerDetailsObject);
	public List<Map<String, Object>> getOffer(UserDetailsObject  userdetails);
	
}
