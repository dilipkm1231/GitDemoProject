package com.loanapp.DaoImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.loanapp.Dao.CreateOfferDao;
import com.loanapp.model.OfferDetailsObject;
@Repository
public class createOfferDaoImpl implements CreateOfferDao {
	@Autowired
	JdbcTemplate jdbcTemplate; 
	private static final String selectOffer = "select income, loanAmount, loanTenure, roi, emi from loanoffers where income<=?";

	
	@Override
	public OfferDetailsObject createOffer(OfferDetailsObject offerDetailsObject) {
		System.out.println("finally in DAOIMpl Layer");
		return null;
	}
	
	
	@Override
	public List<Map<String, Object>> getOffer(String totalIncome) {
		return jdbcTemplate.queryForList(selectOffer, totalIncome);
	}	
	
		


}
